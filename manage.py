#!/usr/bin/env python
import multiprocessing
import os
import sys

if __name__ == "__main__":
    # We make use of multiprocessing (on the controllers subpackage).
    # Support freeze so that we can package this with PyInstaller.
    multiprocessing.freeze_support()
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "visorgen.settings")
    try:
        from django.core.management import execute_from_command_line
    except ImportError:
        # The above import may fail for some other reason. Ensure that the
        # issue is really that Django is missing to avoid masking other
        # exceptions on Python 2.
        try:
            import django
        except ImportError:
            raise ImportError(
                "Couldn't import Django. Are you sure it's installed and "
                "available on your PYTHONPATH environment variable? Did you "
                "forget to activate a virtual environment?"
            )
        raise
    execute_from_command_line(sys.argv)
