from django.conf.urls import include, url
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# import main class intances
from visorgen.mains import user_pages, admin_pages, api_functions


_visorgen_urlpatterns = [

    # User pages

    url(r'^$', user_pages.site_index, name='index'),
    url(r'^searchproc_qstr$', user_pages.search_process, name='search_process'),
    url(r'^waitforit$', user_pages.waitforit_process, name='wait_for_it'),
    url(r'^searchres$', user_pages.searchres, name='searchres'),
    url(r'^searchreslist$', user_pages.searchreslist, name='searchreslist'),
    url(r'^searchresroislist$', user_pages.searchresroislist, name='searchresroislist'),
    url(r'^viewdetails$', user_pages.viewdetails, name='viewdetails'),
    url(r'^trainingimages$', user_pages.get_trainingimages, name='trainingimages'),
    url(r'^nobackend', user_pages.nobackend, name='nobackend'),
    url(r'^accounts/profile/$', user_pages.user_profile, name='user_profile'),
    url(r'^selectpageimages$', user_pages.selectpageimages, name='selectpageimages'),

    # Admin pages
    url(r'^admintools', admin_pages.admintools, name='admintools'),

    # API urls (public)

    url(r'^save_uber_classifier$', api_functions.save_uber_classifier, name='save_uber_classifier'),
    url(r'^is_backend_reachable', api_functions.get_backend_reachable, name='backendreachable'),
    url(r'^execquery$', api_functions.exec_query, name='exec_query'),
    url(r'^uploadimage$', api_functions.upload_image, name='uploadimage'),
    url(r'^(?P<img_set>thumbnails|datasets|postrainimgs|curatedtrainimgs|uploadedimgs|regions)/(.*/)', api_functions.get_image, name='getimage'),
    url(r'^text_suggestions', api_functions.get_text_suggestions, name='text_suggestions'),
    url(r'^save_results_as_via_csv$', api_functions.save_results_as_via_csv, name='save_results_as_via_csv'),
    url(r'^keyword_list$', api_functions.get_keyword_list, name='keyword_list'),

    # API urls (restricted - requires user authentication)

    url(r'^set_config$', api_functions.set_config, name='set_config'),
    url(r'^delete_text_query$', api_functions.delete_text_query, name='delete_text_query'),
    url(r'^clear_cache$', api_functions.clear_cache, name='clear_cache'),
    url(r'^pipeline_input$', api_functions.pipeline_input, name='pipeline_input'),
    url(r'^pipeline_input_status$', api_functions.pipeline_input_status, name='pipeline_input_status'),
    url(r'^pipeline_start$', api_functions.pipeline_start, name='pipeline_start'),
    url(r'^pipeline_status$', api_functions.pipeline_status, name='pipeline_status'),
    url(r'^start_backend$', api_functions.start_backend, name='start_backend'),
    url(r'^stop_backend$', api_functions.stop_backend, name='stop_backend'),
    url(r'^clear_backend$', api_functions.clear_backend, name='clear_backend'),
    url(r'^metadata_reset$', api_functions.metadata_reset, name='metadata_reset'),
]

if settings.SITE_PREFIX:
    assert settings.SITE_PREFIX.startswith('/')
    urlpatterns = [
        url('^' + settings.SITE_PREFIX[1:] + '/',
            include(_visorgen_urlpatterns))
    ]
else:
    urlpatterns = _visorgen_urlpatterns


# FIXME: this adding of static to url patterns is not recommended by
# upstream and we don't actually need it.  However, when we build an
# executable with PyInstaller, the web server no longer finds the
# static files unless we do this (see issue #23).  I guess we should
# be able to use PyInstaller hooks but I couldn't figure the right
# incantations to make it work.
urlpatterns += staticfiles_urlpatterns()


# override 404 handler to show customised page
handler404 = 'visorgen.views.errors.page_not_found'
