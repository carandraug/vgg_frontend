$(function() {
    $('.result_image_link').bind('click', function() {

    div_ele = $(this).parent()

    anno = div_ele.attr('anno')
    if (anno == '+1' || anno == '1') {
        div_ele.attr('anno', '0')
        div_ele.removeClass('roi_box_positive');
        div_ele.addClass('roi_box_skip');
    }
    if (anno == 0) {
        div_ele.attr('anno', '1');
        div_ele.removeClass('roi_box_skip');
        div_ele.addClass('roi_box_positive');
    }

    });

    $('#selection_search').bind('click', function() {
        var query = '';
        var dsetname = '';
        var qtype = '';
        $('.roi_box').each(function(i, obj) {
            if ($(this).attr('anno') == 1 ) {
                query = query + $(this).attr('id') + ',anno:' + $(this).attr('anno') + ';';
                dsetname = $(this).attr('dsetname');
                qtype = $(this).attr('qtype');
                engine = $(this).attr('engine');
            }
        });
        query = query.substr(0,query.length-1);
        startQuery(engine, query, dsetname, qtype);
    });

});

function startQuery(engine, query, dsetname, prev_qtype) {

    if (prev_qtype == 'text' || prev_qtype == 'refine') {
        qtype = 'refine';
    } else if (prev_qtype == 'dsetimage') {
        qtype = 'dsetimage';
    } else if (prev_qtype == 'image') {
        qtype = 'image';
    } else {
        alert('qtype not supported' + qtype);
        return;
    }
    fullHomeLocation = location.protocol + '//' + window.location.hostname;
    if (location.port.length>0) {
        fullHomeLocation = fullHomeLocation + ':' + location.port + '/';
    }
    else {
        fullHomeLocation = fullHomeLocation + '/';
    }
    // Check if there is any site prefix to add to the request.
    const last_slash = location.pathname.lastIndexOf('/');
    if (last_slash >= 1)
        fullHomeLocation += location.pathname.substring(1, last_slash +1);

    execstr = fullHomeLocation + 'searchproc_qstr?q=' + encodeURIComponent(query) +
        '&engine=' + engine +
        '&qtype=' + qtype +
        '&dsetname=' + dsetname;
    var prev_qsid = $('#rpQuerySesId').html();
    if (prev_qsid) {
        execstr += '&prev_qsid=' + prev_qsid;
    }
    location.replace(execstr);
}
